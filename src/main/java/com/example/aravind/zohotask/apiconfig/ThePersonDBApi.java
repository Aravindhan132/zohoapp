package com.example.aravind.zohotask.apiconfig;

import android.content.Context;


import com.example.aravind.zohotask.appConstants.ThePersonDBConstants;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by aravindhan Software on 06/28/18
 */

public class ThePersonDBApi {
    private static Retrofit mRetrofit = null;
    private static OkHttpClient buildClient() {
        return new OkHttpClient
                .Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
    }
    public static Retrofit getClient(Context context) {
        if (mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .client(buildClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(ThePersonDBConstants.BASE_URL)
                    .build();
        }
        return mRetrofit;
    }
}
