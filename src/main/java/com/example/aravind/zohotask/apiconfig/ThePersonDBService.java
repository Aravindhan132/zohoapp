package com.example.aravind.zohotask.apiconfig;


import com.example.aravind.zohotask.TopResults;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by aravindhan Software on 06/28/18
 */

public interface ThePersonDBService {
    @GET("users")
    Call<TopResults> getTopRatedMovies(

            @Query("page") int pageIndex
    );
}
