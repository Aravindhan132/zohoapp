package com.example.aravind.zohotask;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.example.aravind.zohotask.apiconfig.ThePersonDBApi;
import com.example.aravind.zohotask.apiconfig.ThePersonDBService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by aravindhan Software on 06/28/18
 */

public class MainActivity extends AppCompatActivity {
    private final String TAG = MainActivity.class.getSimpleName();
    private PersonListAdapter personListAdapter;
    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerView;

    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 4;
    private int currentPage = PAGE_START;

    private ThePersonDBService theMovieDBService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        mProgressBar = findViewById(R.id.main_progress);
        mRecyclerView = findViewById(R.id.rv_movie_list);

        personListAdapter = new PersonListAdapter(this);
        LinearLayoutManager linearLayoutManager =new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(personListAdapter);

        mRecyclerView.addOnScrollListener(new EndlessScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadMore();
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


        theMovieDBService = ThePersonDBApi.getClient(this).create(ThePersonDBService.class);
        load();
    }
    private void load() {
        Log.d(TAG, "loadFirstPage: ");

        callTopRatedMoviesApi().enqueue(new Callback<TopResults>() {
            @Override
            public void onResponse(Call<TopResults> call, Response<TopResults> response) {
                // Got data. Send it to adapter

                Log.e(TAG, "onResponse: loadfirst"+ response.isSuccessful() );
                if (response.isSuccessful()){
                    List<Result> results = fetchResults(response);
                    mProgressBar.setVisibility(View.GONE);
                    personListAdapter.addAll(results);

                    if (currentPage <= TOTAL_PAGES) personListAdapter.addLoadingFooter();
                    else isLastPage = true;
                }else{
                    Log.e(TAG, "onResponse: not succesfully"+ response.errorBody() );
                }

            }

            @Override
            public void onFailure(Call<TopResults> call, Throwable t) {
                t.printStackTrace();
                Log.e(TAG, "onFailure: "+ t.getLocalizedMessage());
            }
        });

    }

    private void loadMore() {
        Log.d(TAG, "loadNextPage: " + currentPage);

        callTopRatedMoviesApi().enqueue(new Callback<TopResults>() {
            @Override
            public void onResponse(Call<TopResults> call, Response<TopResults> response) {
                personListAdapter.removeLoadingFooter();
                isLoading = false;


                Log.e(TAG, "onResponse: loadfirst"+ response.isSuccessful() );
                if (response.isSuccessful()){
                    List<Result> results = fetchResults(response);
                    mProgressBar.setVisibility(View.GONE);
                    personListAdapter.addAll(results);

                    if (currentPage <= TOTAL_PAGES) personListAdapter.addLoadingFooter();
                    else isLastPage = true;
                }else{
                    Log.e(TAG, "onResponse: not succesfully"+ response.headers().get("status_message") );
                }
            }

            @Override
            public void onFailure(Call<TopResults> call, Throwable t) {
                t.printStackTrace();
                Log.e(TAG, "onFailure: "+ t.getLocalizedMessage());
            }
        });
    }
    private Call<TopResults> callTopRatedMoviesApi() {
        return theMovieDBService.getTopRatedMovies(

                currentPage
        );
    }


    private List<Result> fetchResults(Response<TopResults> response) {
        TopResults topRatedMovies = response.body();
        return topRatedMovies.getResults();
    }
}
